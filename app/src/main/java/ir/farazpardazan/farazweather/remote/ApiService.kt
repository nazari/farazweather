package ir.farazpardazan.farazweather.remote

import io.reactivex.rxjava3.core.Single
import ir.farazpardazan.farazweather.model.weatherbycity.response.ResponseOneCall
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
//    current weather
//    http://api.openweathermap.org/data/2.5/weather?q=Tehran,IR&APPID=c02c53810c03d0d4fcd6d74658d27263}

    @GET("/data/2.5/onecall")
    fun getWeatherByCity(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") apiKey: String,
        @Query("units") unit: String
        ): Single<ResponseOneCall>
}