package ir.farazpardazan.farazweather.remote

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import ir.farazpardazan.farazweather.utils.API_KEY
import ir.farazpardazan.farazweather.utils.UNIT
import ir.farazpardazan.farazweather.model.weatherbycity.response.ResponseOneCall

class ApiRepository(private val apiService: ApiService){

    fun getWeatherByCity(lat:Double,long:Double):Single<ResponseOneCall>{
       return apiService.getWeatherByCity(lat,long,
           API_KEY,
           UNIT
       )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}