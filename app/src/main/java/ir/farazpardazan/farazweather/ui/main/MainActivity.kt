package ir.farazpardazan.farazweather.ui.main

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import ir.farazpardazan.farazweather.R
import ir.farazpardazan.farazweather.databinding.ActivityMainBinding
import ir.farazpardazan.farazweather.model.citylist.City
import ir.farazpardazan.farazweather.model.weatherbycity.response.Daily
import ir.farazpardazan.farazweather.model.weatherbycity.response.Hourly
import ir.farazpardazan.farazweather.ui.citydialog.CityDialogFragment
import ir.farazpardazan.farazweather.ui.main.adapter.DailyWeatherAdapter
import ir.farazpardazan.farazweather.ui.main.adapter.HourlyWeatherAdapter
import ir.farazpardazan.farazweather.ui.main.adapter.ItemListener
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity() ,ItemListener,SelectCityListener{

    private val viewModel: MainViewModel by inject()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        observeData()

        handClicks()

    }

    private fun handClicks() {
        binding.btnCity.setOnClickListener {
            openCityDialog()
        }
    }

    private fun observeData() {

        viewModel.weatherResponse.observe(this, Observer {
            it.daily?.let { dailyList -> initDailyRecycler(dailyList) }
            it.hourly?.let { dailyList -> initHourlyRecycler(dailyList) }
        })

        viewModel.errorMessage.observe(this, Observer {
            if (it.isNotEmpty()){
                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
//                viewModel.clearErrorMessage()
            }
        })

    }



    private fun openCityDialog() {
        CityDialogFragment.newInstance(this).show(supportFragmentManager,"CityDialog")
    }


    private fun initHourlyRecycler(listHourly: List<Hourly>) {
        val adapter = HourlyWeatherAdapter()
        adapter.submitList(listHourly)
        binding.rvHourly.adapter = adapter
    }

    private fun initDailyRecycler(listDaily: List<Daily>) {
        val adapter = DailyWeatherAdapter(this)
        adapter.submitList(listDaily)
        binding.rvDaily.adapter = adapter
    }

    override fun clickWeatherItem(dailyItem: Daily) {
        viewModel.updateCurrentTemp(dailyItem)
        viewModel.updateMainImage(dailyItem)
    }

    override fun onSelectCity(selectedCity:City) {
        viewModel.selectedCity.value = selectedCity
        viewModel.getWeatherData()
    }

}