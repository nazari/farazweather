package ir.farazpardazan.farazweather.ui.citydialog

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.farazpardazan.farazweather.databinding.ItemRecyclerCityBinding
import ir.farazpardazan.farazweather.model.citylist.City

class CityAdapter (val itemListener: CityItemListener) : ListAdapter<City, CityAdapter.CityViewHolder>(
    ItemCallBack()
){



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        val binding = ItemRecyclerCityBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return CityViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.binding.item = getItem(position)
        holder.binding.root.setOnClickListener { itemListener.clickItem(getItem(position)) }
    }

    class  CityViewHolder(val binding: ItemRecyclerCityBinding): RecyclerView.ViewHolder(binding.root)


}

class ItemCallBack : DiffUtil.ItemCallback<City>(){
    override fun areItemsTheSame(oldItem: City, newItem: City): Boolean {
        return oldItem.city == newItem.city
    }

    override fun areContentsTheSame(oldItem: City, newItem: City): Boolean {
        return oldItem == newItem
    }

}


interface CityItemListener {
    fun clickItem(cityItem: City)
}
