package ir.farazpardazan.farazweather.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.farazpardazan.farazweather.databinding.ItemRecyclerHourlyBinding
import ir.farazpardazan.farazweather.model.weatherbycity.response.Hourly

class HourlyWeatherAdapter : ListAdapter<Hourly, HourlyWeatherAdapter.HourlyViewHolder>(
    HourlyItemCallBack()
){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HourlyViewHolder {
        val binding = ItemRecyclerHourlyBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return HourlyViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: HourlyViewHolder, position: Int) {
        holder.binding.item = getItem(position)
    }

    class  HourlyViewHolder(val binding: ItemRecyclerHourlyBinding): RecyclerView.ViewHolder(binding.root)


}

class HourlyItemCallBack : DiffUtil.ItemCallback<Hourly>(){
    override fun areItemsTheSame(oldItem: Hourly, newItem: Hourly): Boolean {
        return oldItem.dt == newItem.dt
    }

    override fun areContentsTheSame(oldItem: Hourly, newItem: Hourly): Boolean {
        return oldItem == newItem
    }

}