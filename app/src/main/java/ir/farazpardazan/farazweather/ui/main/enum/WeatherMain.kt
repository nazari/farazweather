package ir.farazpardazan.farazweather.ui.main.enum

enum class WeatherMain(val label: String) {
    CLEAR("clear")
    ,
    BROKEN("Clouds")
    ,
    FEW_CLOUD("Clouds")
    ,
    MIST("Mist")
    ,
    RAIN("Rain")
    ,
    SCATTED_CLOUDS("scatted_clouds")
    ,
    SHOWER_RAIN("Drizzle")
    ,
    SNOW("Snow")
    ,
    THUNDERSTORM("Thunderstorm")
}