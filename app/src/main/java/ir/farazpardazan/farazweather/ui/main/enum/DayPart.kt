package ir.farazpardazan.farazweather.ui.main.enum

enum class DayPart(val hourRange:IntRange) {
    DAWN(0..6),
    MORNING(7..11),
    NOON(12..16),
    EVENING(17..19),
    NIGHT(20..23)
}