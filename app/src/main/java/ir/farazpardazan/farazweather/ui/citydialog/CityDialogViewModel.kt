package ir.farazpardazan.farazweather.ui.citydialog

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import ir.farazpardazan.farazweather.model.citylist.City
import ir.farazpardazan.farazweather.model.citylist.ResponseCity

class CityDialogViewModel(moshi: Moshi, context: Application) : AndroidViewModel(context) {

    val cityList = MutableLiveData<List<City>>()

    init {
        cityList.value = getCityList(moshi, context)?.cityList
    }

    private fun getCityList(moshi: Moshi, context: Application): ResponseCity? {
        val adapter: JsonAdapter<ResponseCity> = moshi.adapter(ResponseCity::class.java)
        val citiesFile = "cities.json"
        val jsonFile = context.assets.open(citiesFile).bufferedReader().use { it.readText() }
        return adapter.fromJson(jsonFile)
    }


}