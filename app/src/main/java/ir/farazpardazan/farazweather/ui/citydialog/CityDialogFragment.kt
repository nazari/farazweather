package ir.farazpardazan.farazweather.ui.citydialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import ir.farazpardazan.farazweather.databinding.CityDialogFragmentBinding
import ir.farazpardazan.farazweather.model.citylist.City
import ir.farazpardazan.farazweather.ui.main.SelectCityListener
import org.koin.android.ext.android.inject

class CityDialogFragment : DialogFragment(), CityItemListener {

    companion object {
        lateinit var selectCityListener: SelectCityListener

        fun newInstance(listener: SelectCityListener): CityDialogFragment {
            this.selectCityListener = listener
            return CityDialogFragment()
        }
    }

    private val viewModel: CityDialogViewModel by inject()
    lateinit var binding: CityDialogFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = CityDialogFragmentBinding.inflate(inflater)

        viewModel.cityList.observe(viewLifecycleOwner, Observer {
            it?.let { it1 -> initRecycler(it1) }
        })


        return binding.root

    }

    private fun initRecycler(cityList: List<City>) {
        val adapter = CityAdapter(this)
        adapter.submitList(cityList)
        binding.rvCity.adapter = adapter
    }


    override fun clickItem(item: City) {
        if (item.lat != null && item.long != null)
            selectCityListener.onSelectCity(item)

        dismiss()
    }

}
