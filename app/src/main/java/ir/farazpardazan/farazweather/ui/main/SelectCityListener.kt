package ir.farazpardazan.farazweather.ui.main

import ir.farazpardazan.farazweather.model.citylist.City

interface SelectCityListener {
    fun onSelectCity(selectedCty: City)
}