package ir.farazpardazan.farazweather.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.farazpardazan.farazweather.databinding.ItemRecyclerDailyBinding
import ir.farazpardazan.farazweather.model.weatherbycity.response.Daily

class DailyWeatherAdapter(val itemListener: ItemListener) : ListAdapter<Daily, DailyWeatherAdapter.DailyViewHolder>(
    ItemCallBack()
){



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DailyViewHolder {
        val binding = ItemRecyclerDailyBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return DailyViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: DailyViewHolder, position: Int) {
        holder.binding.item = getItem(position)
        holder.binding.constraintRoot.setOnClickListener {
            itemListener.clickWeatherItem(getItem(position))
        }
    }

    class  DailyViewHolder(val binding:ItemRecyclerDailyBinding): RecyclerView.ViewHolder(binding.root)


}

class ItemCallBack : DiffUtil.ItemCallback<Daily>(){
    override fun areItemsTheSame(oldItem: Daily, newItem: Daily): Boolean {
        return oldItem.dt == newItem.dt
    }

    override fun areContentsTheSame(oldItem: Daily, newItem: Daily): Boolean {
        return oldItem == newItem
    }

}

interface ItemListener{
    fun clickWeatherItem(dailyItem:Daily)
}