package ir.farazpardazan.farazweather.ui.main

import android.app.Application
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import ir.farazpardazan.farazweather.model.citylist.City
import ir.farazpardazan.farazweather.model.weatherbycity.response.Daily
import ir.farazpardazan.farazweather.model.weatherbycity.response.ResponseOneCall
import ir.farazpardazan.farazweather.remote.ApiRepository
import ir.farazpardazan.farazweather.ui.main.enum.DayPart
import ir.farazpardazan.farazweather.utils.getColorByString
import ir.farazpardazan.farazweather.utils.getCurrentTime
import ir.farazpardazan.farazweather.utils.getDayPartWithTime
import ir.farazpardazan.farazweather.utils.getWeatherMainImage
import timber.log.Timber
import java.util.*

class MainViewModel(private val apiRepository: ApiRepository, private val context: Application) :
    AndroidViewModel(context) {

    val gradientDrawable = MutableLiveData<GradientDrawable>()
    val selectedCity = MutableLiveData(City("Tehran", 35.6892, 51.3890))
    private val _weatherResponse = MutableLiveData<ResponseOneCall>()
    val weatherResponse: LiveData<ResponseOneCall>
        get() = _weatherResponse

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    val isShowLoading = MutableLiveData(true)
    val isShowSmallLoading = MutableLiveData(false)
    val isShowRetry = MutableLiveData(false)

    init {
        isShowLoading.value = true
        getWeatherData()
        getGradientColor()

    }

    val mainImage: MutableLiveData<Int?> = Transformations.map(weatherResponse) {
        it?.current?.weather?.get(0)?.main?.let { image -> getWeatherMainImage(image) }
    } as MutableLiveData<Int?>

    val currentTemp: MutableLiveData<Int?> = Transformations.map(weatherResponse) {
        it?.current?.temp?.toInt()
    } as MutableLiveData<Int?>

    fun getWeatherData() {
        isShowSmallLoading.value = true
        if (selectedCity.value?.lat != null && selectedCity.value?.long != null)
            apiRepository.getWeatherByCity(
                selectedCity.value?.lat ?: 35.6892,
                selectedCity.value?.long ?: 51.3890
            )
                .subscribe(
                    {
                        isShowLoading.value = false
                        isShowSmallLoading.value = false
                        _weatherResponse.value = it
                    },
                    {
                        isShowLoading.value = false
                        isShowSmallLoading.value = false
                        Timber.e(it)
                        _errorMessage.value = it.message
                        isShowRetry.value = true
                    })
    }

    private fun getGradientColor() {
        val currentDayPart = getDayPartWithTime(getCurrentTime()).name.toLowerCase(Locale.ROOT)
        val startColor = getColorByString(context, "${currentDayPart}TopGradientColor")
        val endColor = getColorByString(context, "${currentDayPart}BottomGradientColor")
        val gd = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(
                ContextCompat.getColor(context, startColor),
                ContextCompat.getColor(context, endColor)
            )
        )
        gd.cornerRadius = 0f

        gradientDrawable.value = gd
    }

    fun updateMainImage(dailyItem: Daily) {
        mainImage.value =
            dailyItem.weather?.get(0)?.main?.let { image -> getWeatherMainImage(image) }

    }

    fun updateCurrentTemp(dailyItem: Daily) {
        currentTemp.value = when (getDayPartWithTime(getCurrentTime())) {
            DayPart.DAWN -> dailyItem.temp?.min?.toInt()
            DayPart.MORNING -> dailyItem.temp?.morn?.toInt()
            DayPart.NIGHT -> dailyItem.temp?.night?.toInt()
            DayPart.NOON -> dailyItem.temp?.max?.toInt()
            DayPart.EVENING -> dailyItem.temp?.eve?.toInt()
        }
    }

    fun retry() {
        getWeatherData()
        isShowRetry.value = false
        isShowLoading.value=true
    }

}