package ir.farazpardazan.farazweather.model.citylist


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class ResponseCity(
    @Json(name = "cityList")
    val cityList: List<City>?
) : Parcelable