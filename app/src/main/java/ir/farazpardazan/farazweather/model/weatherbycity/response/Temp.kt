package ir.farazpardazan.farazweather.model.weatherbycity.response


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Temp(
    @Json(name = "day")
    val day: Double?,
    @Json(name = "eve")
    val eve: Double?,
    @Json(name = "max")
    val max: Double?,
    val intMax: Int? = max?.toInt(),
    @Json(name = "min")
    val min: Double?,
    val intMin: Int? = min?.toInt(),
    @Json(name = "morn")
    val morn: Double?,
    @Json(name = "night")
    val night: Double?
) : Parcelable