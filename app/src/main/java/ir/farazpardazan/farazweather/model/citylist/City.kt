package ir.farazpardazan.farazweather.model.citylist


import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class City(
    @Json(name = "city")
    val city: String?,
    @Json(name = "lat")
    val lat: Double?,
    @Json(name = "long")
    val long: Double?
) : Parcelable