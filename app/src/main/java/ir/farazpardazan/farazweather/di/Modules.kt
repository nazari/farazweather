package ir.farazpardazan.farazweather.di

import ir.farazpardazan.farazweather.remote.ApiRepository
import ir.farazpardazan.farazweather.ui.citydialog.CityDialogViewModel
import ir.farazpardazan.farazweather.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val networkModule = module {

    single { provideOkHttpClient() }
    single { provideApiService(get()) }
    single { provideRetrofit(get(), get()) }
    factory { ApiRepository(get()) }
    single { provideMoshi() }
}

val viewModelModules = module {
    viewModel { MainViewModel(get(), get()) }
    viewModel { CityDialogViewModel(get(), get()) }
}