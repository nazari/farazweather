package ir.farazpardazan.farazweather.utils

import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import ir.farazpardazan.farazweather.R
import kotlin.math.roundToInt


@BindingAdapter("isVisible")
fun View.isVisible(isVisible: Boolean) {
    if (isVisible) this.visibility = View.VISIBLE else this.visibility = View.INVISIBLE
}

@BindingAdapter("isGone")
fun View.isGone(isGone: Boolean) {
    if (isGone) this.visibility = View.GONE else this.visibility = View.VISIBLE
}

@BindingAdapter("tempText")
fun TextView.tempText(temp: Double) {
   this.text =   this.context.getString(R.string.celisius_temp,temp.roundToInt())
}

@BindingAdapter("android:src")
fun setImageUri(view: ImageView, imageUri: String?) {
    if (imageUri == null) {
        view.setImageURI(null)
    } else {
        view.setImageURI(Uri.parse(imageUri))
    }
}

@BindingAdapter("android:src")
fun setImageUri(view: ImageView, imageUri: Uri?) {
    view.setImageURI(imageUri)
}

@BindingAdapter("android:src")
fun setImageDrawable(
    view: ImageView,
    drawable: Drawable?
) {
    view.setImageDrawable(drawable)
}

@BindingAdapter("android:src")
fun setImageResource(imageView: ImageView, resource: Int) {
    imageView.setImageResource(resource)
}