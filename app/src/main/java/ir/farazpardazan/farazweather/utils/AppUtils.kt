package ir.farazpardazan.farazweather.utils

import android.content.Context
import ir.farazpardazan.farazweather.R
import ir.farazpardazan.farazweather.ui.main.enum.DayPart
import ir.farazpardazan.farazweather.ui.main.enum.WeatherMain
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


fun getWeatherMainImage(main: String): Int {
    val time = getCurrentTime()
    val currentDayPart = getDayPartWithTime(time)
    return when (main) {
        WeatherMain.CLEAR.label -> when (currentDayPart) {
            DayPart.DAWN -> R.drawable.ic_weather_clear_sky_dawn
            DayPart.EVENING -> R.drawable.ic_weather_clear_sky_evening
            DayPart.MORNING -> R.drawable.ic_weather_clear_sky_morning
            else -> R.drawable.ic_weather_clear_sky_night
        }
        WeatherMain.FEW_CLOUD.label -> when (currentDayPart) {
            DayPart.DAWN -> R.drawable.ic_weather_few_cloud_dawn
            DayPart.EVENING -> R.drawable.ic_weather_few_cloud_dawn
            DayPart.MORNING -> R.drawable.ic_weather_few_cloud_dawn
            else -> R.drawable.ic_weather_few_cloud_dawn
        }
        WeatherMain.BROKEN.label -> R.drawable.ic_weather_few_cloud_dawn
        WeatherMain.SNOW.label -> R.drawable.ic_weather_snow
        WeatherMain.THUNDERSTORM.label -> R.drawable.ic_weather_thunderstorm
        WeatherMain.SHOWER_RAIN.label -> R.drawable.ic_weather_shower_raint
        WeatherMain.SCATTED_CLOUDS.label -> R.drawable.ic_weather_scattered_clouds
        WeatherMain.RAIN.label -> R.drawable.ic_weather_rain
        WeatherMain.MIST.label -> R.drawable.ic_weather_mist
        else -> R.drawable.ic_weather_clear_sky_dawn
    }

}

fun getCurrentTime(): Int {
    val rightNow: Calendar = Calendar.getInstance()
    return rightNow.get(Calendar.HOUR_OF_DAY)
}

fun getDayPartWithTime(currentHour: Int): DayPart {

    return when (currentHour) {
        in DayPart.DAWN.hourRange -> DayPart.DAWN
        in DayPart.EVENING.hourRange -> DayPart.EVENING
        in DayPart.MORNING.hourRange -> DayPart.MORNING
        in DayPart.NIGHT.hourRange -> DayPart.NIGHT
        else -> DayPart.NOON

    }

}

fun formatTimeToHour(timeStamp: Long): String {

    // Creating date format
    val locale = Locale.getDefault()
    val simple: DateFormat = SimpleDateFormat("HH a", locale)
    val result = Date(timeStamp * 1000)
    return simple.format(result)
}

fun formatTimeToWeekDay(timeStampSec: Long): String {
    // Creating date format
    return when {
        isToday(timeStampSec) -> {
            "Today"
        }
        isTomorrow(timeStampSec) -> {
            "Tomorrow"
        }
        else -> {
            val locale = Locale.getDefault()
            val simple: DateFormat = SimpleDateFormat("EEEE", locale)
            val result = Date(timeStampSec * 1000)
            simple.format(result)
        }
    }

}

fun isToday(timeStampSec: Long): Boolean {
    val now = Calendar.getInstance()
    val timeToCheck = Calendar.getInstance()
    timeToCheck.timeInMillis = timeStampSec * 1000

    if (now[Calendar.YEAR] == timeToCheck[Calendar.YEAR]) {
        if (now[Calendar.DAY_OF_YEAR] == timeToCheck[Calendar.DAY_OF_YEAR]) {
            return true
        }
    }
    return false
}

fun isTomorrow(timeStampSec: Long): Boolean {
    val now = Calendar.getInstance()
    val timeToCheck = Calendar.getInstance()
    timeToCheck.timeInMillis = timeStampSec * 1000

    if (now[Calendar.YEAR] == timeToCheck[Calendar.YEAR]) {
        if (now[Calendar.DAY_OF_YEAR] + 1 == timeToCheck[Calendar.DAY_OF_YEAR]) {
            return true
        }
    }
    return false
}


// get Color Resource with string
fun getColorByString(context: Context, colorName: String): Int {
    try {
        return context.resources.getIdentifier(
            colorName,
            "color",
            context.packageName
        )
    }catch (e:Exception){
        e.printStackTrace()
        return R.color.colorAccent
    }

}
